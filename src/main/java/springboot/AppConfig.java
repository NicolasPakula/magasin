package springboot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springboot.repositories.CategorieRepository;
import springboot.repositories.ClientRepository;
import springboot.repositories.CommandeRepository;
import springboot.repositories.ProduitRepository;
import springboot.services.CategorieService;
import springboot.services.CategorieServiceImpl;
import springboot.services.ClientService;
import springboot.services.ClientServiceImpl;
import springboot.services.CommandeService;
import springboot.services.CommandeServiceImpl;
import springboot.services.ProduitService;
import springboot.services.ProduitServiceImpl;

@Configuration
public class AppConfig {
	@Bean
	public CategorieService categorieService(CategorieRepository categorieRepository, ProduitRepository produitRepository) {
		return new CategorieServiceImpl(categorieRepository,produitRepository);
	}
	
	@Bean
	public ClientService clientService(ClientRepository clientRepository, CommandeRepository commandeRepository) {
		return new ClientServiceImpl(clientRepository,commandeRepository);
	}
	
	@Bean
	public ProduitService produitService(ProduitRepository produitRepository) {
		return new ProduitServiceImpl(produitRepository);
	}
	
	@Bean
	public CommandeService commandeService(CommandeRepository commandeRepository) {
		return new CommandeServiceImpl(commandeRepository);
	}
}
