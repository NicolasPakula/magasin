package springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Categorie;
import springboot.models.Produit;
import springboot.services.CategorieService;

@RestController
@CrossOrigin
@RequestMapping("categories")
public class CategorieController {
	@Autowired
	private CategorieService categorieService;
	
	@GetMapping
	public List<Categorie> getAll(){
		return this.categorieService.getAll();
	}
	
	@PostMapping
	public Categorie create(@RequestBody Categorie categorie) {
		return this.categorieService.create(categorie);
	}
	
	@GetMapping("{id}/produits")
	public List<Produit> findProduitByCategory(@PathVariable Long id){
		return this.categorieService.findProduitByCategorie(id);
	}
	
	@PutMapping
	public Categorie put(@RequestBody Categorie categorie) {
		return this.categorieService.put(categorie);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Long id) {
		this.categorieService.delete(id);
	}
	
	@GetMapping("{id}")
	public Categorie getById(@PathVariable Long id) {
		return this.categorieService.getById(id);
	}
}
