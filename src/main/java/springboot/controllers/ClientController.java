package springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Client;
import springboot.models.Commande;
import springboot.models.Produit;
import springboot.services.ClientService;

@RestController
@CrossOrigin
@RequestMapping("clients")
public class ClientController {
	@Autowired
	private ClientService clientService;
	@GetMapping
	public List<Client> getAll(){
		return this.clientService.getAll();
	}
	
	@PostMapping
	public Client create(@RequestBody Client client) {
		return this.clientService.create(client);
	}
	
	@GetMapping("{id}/commandes")
	public List<Commande> findCommandeByProduitId(@PathVariable Long id){
		return this.clientService.findCommandeByProduitId(id);
	}
	
	@PutMapping
	public Client put(@RequestBody Client client) {
		return this.clientService.put(client);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Long id) {
		this.clientService.delete(id);
	}
	
	@GetMapping("{id}")
	public Client getById(@PathVariable Long id) {
		return this.clientService.getById(id);
	}
}
