package springboot.controllers;

import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Commande;
import springboot.services.CommandeService;

@RestController
@CrossOrigin
@RequestMapping("commandes")
public class CommandeController {
	@Autowired
	private CommandeService commandeService;
	
	@GetMapping
	public List<Commande> getAll(){
		return this.commandeService.getAll();
	}
	
	@PostMapping
	public Commande create(@RequestBody Commande commande) {
		return this.commandeService.create(commande);
	}
	
	@PutMapping
	public Commande put(@RequestBody Commande commande) {
		return this.commandeService.put(commande);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Long id) {
		this.commandeService.delete(id);
	}
	
	@GetMapping("{id}")
	public Commande getById(@PathVariable Long id) {
		return this.commandeService.getById(id);
	}
	
}
