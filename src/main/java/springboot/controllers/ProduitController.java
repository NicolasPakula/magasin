package springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Commande;
import springboot.models.Produit;
import springboot.services.ProduitService;

@RestController
@CrossOrigin
@RequestMapping("produits")
public class ProduitController {
	@Autowired
	private ProduitService produitService;
	
	@GetMapping
	public List<Produit> getAll(){
		return this.produitService.getAll();
	}
	
	@PostMapping
	public Produit create(@RequestBody Produit produit) {
		return this.produitService.create(produit);
	}
	
	@PutMapping
	public Produit put(@RequestBody Produit produit) {
		return this.produitService.put(produit);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Long id) {
		this.produitService.delete(id);
	}
	
	@GetMapping("{id}")
	public Produit getById(@PathVariable Long id) {
		return this.produitService.getById(id);
	}
}
