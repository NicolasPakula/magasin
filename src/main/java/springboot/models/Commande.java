package springboot.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.Data;

@Data
@Entity
@Table(name="commande")
public class Commande {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_client")
	private Client client;
	
	@ManyToMany
	@JoinTable(
		name="commande_has_produit",
		joinColumns= {@JoinColumn(name="id_commande")},
		inverseJoinColumns={@JoinColumn(name="id_produit")}
	)
	private List<Produit> produits;
	
}
