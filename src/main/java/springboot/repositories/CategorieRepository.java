package springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import springboot.models.Categorie;

public interface CategorieRepository extends JpaRepository<Categorie, Long>{

}
