package springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import springboot.models.Client;

public interface ClientRepository extends JpaRepository<Client, Long>{

}
