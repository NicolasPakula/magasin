package springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import springboot.models.Commande;

public interface CommandeRepository extends JpaRepository<Commande, Long>{

	public List<Commande> findByClientId(Long id);

}
