package springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import springboot.models.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long>{
	public List<Produit> findByCategorieId(Long id);
}
