package springboot.services;

import java.util.List;

import springboot.models.Categorie;
import springboot.models.Produit;

public interface CategorieService {
	public List<Categorie> getAll();
	public Categorie create(Categorie categorie);
	public List<Produit> findProduitByCategorie(Long id);
	public Categorie put(Categorie categorie);
	public void delete(Long id);
	public Categorie getById(Long id);
}
