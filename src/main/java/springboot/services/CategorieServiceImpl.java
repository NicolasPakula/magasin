package springboot.services;

import java.util.List;

import springboot.models.Categorie;
import springboot.models.Produit;
import springboot.repositories.CategorieRepository;
import springboot.repositories.ProduitRepository;

public class CategorieServiceImpl implements CategorieService{

	private CategorieRepository categorieRepository;
	private ProduitRepository produitRepository;
	
	public CategorieServiceImpl(CategorieRepository categorieRepository, ProduitRepository produitRepository) {
		this.categorieRepository = categorieRepository;
		this.produitRepository = produitRepository;
	}
	
	@Override
	public List<Categorie> getAll() {
		return this.categorieRepository.findAll();
	}

	@Override
	public Categorie create(Categorie categorie) {
		return this.categorieRepository.save(categorie);
	}

	@Override
	public List<Produit> findProduitByCategorie(Long id) {
		return this.produitRepository.findByCategorieId(id);
	}

	@Override
	public Categorie put(Categorie categorie) {
		return this.categorieRepository.save(categorie);
	}

	@Override
	public void delete(Long id) {
		this.categorieRepository.deleteById(id);
	}

	@Override
	public Categorie getById(Long id) {
		return this.categorieRepository.findById(id).orElse(null);
	}
	
	

}
