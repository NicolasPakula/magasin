package springboot.services;

import java.util.List;

import springboot.models.Client;
import springboot.models.Commande;

public interface ClientService {
	public List<Client> getAll();
	public Client create(Client client);
	public List<Commande> findCommandeByProduitId(Long id);
	public Client put(Client client);
	public void delete(Long id);
	public Client getById(Long id);
}
