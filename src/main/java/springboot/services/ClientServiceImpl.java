package springboot.services;

import java.util.List;

import springboot.models.Client;
import springboot.models.Commande;
import springboot.repositories.ClientRepository;
import springboot.repositories.CommandeRepository;

public class ClientServiceImpl implements ClientService{

	private ClientRepository clientRepository;
	private CommandeRepository commandeRepository;
	
	public ClientServiceImpl(ClientRepository clientRepository, CommandeRepository commandeRepository) {
		this.clientRepository = clientRepository;
		this.commandeRepository = commandeRepository;
	}
	
	@Override
	public List<Client> getAll() {
		return this.clientRepository.findAll();
	}

	@Override
	public Client create(Client client) {
		return this.clientRepository.save(client);
	}

	@Override
	public List<Commande> findCommandeByProduitId(Long id) {
		return this.commandeRepository.findByClientId(id);
	}

	@Override
	public Client put(Client client) {
		return this.clientRepository.save(client);
	}

	@Override
	public void delete(Long id) {
		this.clientRepository.deleteById(id);
	}

	@Override
	public Client getById(Long id) {
		return this.clientRepository.findById(id).orElse(null);
	}

}
