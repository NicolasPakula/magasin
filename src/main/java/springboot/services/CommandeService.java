package springboot.services;

import java.util.List;

import springboot.models.Commande;

public interface CommandeService {

	List<Commande> getAll();

	Commande create(Commande commande);

	Commande put(Commande commande);

	void delete(Long id);

	Commande getById(Long id);

}
