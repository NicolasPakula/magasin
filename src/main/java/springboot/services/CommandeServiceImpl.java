package springboot.services;

import java.util.List;

import springboot.models.Commande;
import springboot.repositories.CommandeRepository;

public class CommandeServiceImpl implements CommandeService{
	private CommandeRepository commandeRepository;
	
	public CommandeServiceImpl(CommandeRepository commandeRepository) {
		this.commandeRepository = commandeRepository;
	}

	@Override
	public List<Commande> getAll() {
		return this.commandeRepository.findAll();
	}

	@Override
	public Commande create(Commande commande) {
		return this.commandeRepository.save(commande);
	}

	@Override
	public Commande put(Commande commande) {
		return this.commandeRepository.save(commande);
	}

	@Override
	public void delete(Long id) {
		this.commandeRepository.deleteById(id);
	}

	@Override
	public Commande getById(Long id) {
		return this.commandeRepository.findById(id).orElse(null);
	}
}
