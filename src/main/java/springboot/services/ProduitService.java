package springboot.services;

import java.util.List;

import springboot.models.Produit;

public interface ProduitService {

	public List<Produit> getAll();

	public Produit create(Produit produit);

	public Produit put(Produit produit);

	public void delete(Long id);

	public Produit getById(Long id);

}
