package springboot.services;

import java.util.List;

import springboot.models.Produit;
import springboot.repositories.ProduitRepository;

public class ProduitServiceImpl implements ProduitService{
	private ProduitRepository produitRepository;
	
	public ProduitServiceImpl(ProduitRepository produitRepository) {
		this.produitRepository = produitRepository;
	}

	@Override
	public List<Produit> getAll() {
		return this.produitRepository.findAll();
	}

	@Override
	public Produit create(Produit produit) {
		return this.produitRepository.save(produit);
	}

	@Override
	public Produit put(Produit produit) {
		return this.produitRepository.save(produit);
	}

	@Override
	public void delete(Long id) {
		this.produitRepository.deleteById(id);
	}

	@Override
	public Produit getById(Long id) {
		return this.produitRepository.findById(id).orElse(null);
	}
}
